$> if [ -d /opt/stack ]; then \rm -rf /opt/stack; fi
$> cat > ~/clean_os_db.sql << _EOF
drop database if exists nova;
drop database if exists glance;
drop database if exists cinder;
drop database if exists keystone;
grant usage on *.* to 'nova'@'%'; drop user 'nova'@'%';
grant usage on *.* to 'nova'@'localhost'; drop user 'nova'@'localhost';
grant usage on *.* to 'glance'@'%'; drop user 'glance'@'%';
grant usage on *.* to 'glance'@'localhost'; drop user 'glance'@'localhost';
grant usage on *.* to 'cinder'@'%'; drop user 'cinder'@'%';
grant usage on *.* to 'cinder'@'localhost'; drop user 'cinder'@'localhost';
grant usage on *.* to 'keystone'@'%'; drop user 'keystone'@'%';
grant usage on *.* to 'keystone'@'localhost'; drop user 'keystone'@'localhost';
flush privileges;
_EOF